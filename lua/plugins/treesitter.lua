return {
    "nvim-treesitter/nvim-treesitter",
    config = function()
        require("nvim-treesitter.configs").setup({
            highlight = {
                enable = true
            },
            autopairs = {
                enable = true,
            },
        })
    end,
    build = vim.cmd.TSUpdate,
}
