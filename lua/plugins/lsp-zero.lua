return {
    "VonHeikemen/lsp-zero.nvim",
    branch = "v2.x",
    dependencies = {
        "neovim/nvim-lspconfig",
        "hrsh7th/nvim-cmp",
        "hrsh7th/cmp-nvim-lsp",
        "L3MON4D3/LuaSnip",
        "onsails/lspkind.nvim",
        -- C# decompilation
        "Hoffs/omnisharp-extended-lsp.nvim",
    },
    config = function()
        local lsp = require("lsp-zero").preset({})

        lsp.on_attach(function(_, bufnr)
            lsp.default_keymaps({buffer = bufnr})
        end)
        vim.keymap.set("n", "<leader>h", vim.cmd.ClangdSwitchSourceHeader)

        lsp.setup_servers({
            "clangd",
            "pylsp",
            "omnisharp",
            "rust_analyzer",
            "hls",
            "ts_ls",
            "zls",
            "cssls",
            "html",
            "tailwindcss",
            "volar",
            "gdscript",
            "texlab",
        })

        require("lspconfig").omnisharp.setup({
            cmd = { "omnisharp" },
            handlers = {
                ["textDocument/definition"] = require('omnisharp_extended').handler,
            },
            on_attach = function(client, _)
                -- without this the editor gets flooded with errors
                client.server_capabilities.semanticTokensProvider = nil
            end,
        })

        require("lspconfig").lua_ls.setup(lsp.nvim_lua_ls())

        -- Since we're moving from tsserver to ts_ls,
        -- it's probably better to just comment this out,
        -- rather than leave it and hope it still works.
        -- I don't plan on doing more stuff in vue anyways.
        --[[
        local vue_language_server_path = "/home/bartek/Downloads/apps/nodowe/lib/node_modules/@vue/language-server"
        require('lspconfig').tsserver.setup {
            init_options = {
                plugins = {
                    {
                        name = '@vue/typescript-plugin',
                        location = vue_language_server_path,
                        languages = { 'vue' },
                    },
                },
            },
            filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue' },
        }
        --]]
        require('lspconfig').volar.setup({})

        require("lspconfig").cssls.setup({
            settings = {
                css = {
                    lint = {
                        unknownAtRules = "ignore",
                    },
                },
            },
        })

        lsp.setup()

        local cmp = require("cmp")
        require("cmp").setup({
            preselect = "none",
            completion = {
                completeopt = "menuone,preview,noselect,noinsert",
            },
            window = {
                documentation = cmp.config.window.bordered(),
            },
            formatting = {
                format = require("lspkind").cmp_format({})
            },
            mapping = cmp.mapping.preset.insert({
                ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
                ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
            })
        })
    end,
}
