return {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-tree/nvim-web-devicons",
        "nvim-telescope/telescope-ui-select.nvim",
        { "nvim-telescope/telescope-fzf-native.nvim", build = "make" },
    },
    config = function()
        local telescope = require("telescope")

        telescope.setup({
            extensions = {
                ["ui-select"] = {
                    require("telescope.themes").get_ivy({
                        layout_config = {
                            height = 10,
                        },
                    })
                }
            },
        })

        telescope.load_extension("fzf")
        telescope.load_extension("ui-select")

        local builtin = require("telescope.builtin")
        local keymap = vim.keymap.set

        keymap("n", "<Leader>e", builtin.find_files)
        keymap("n", "<Leader>b", builtin.buffers)
        keymap("n", "<Leader>g", builtin.live_grep)
    end,
}
