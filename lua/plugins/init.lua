-- boorstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    require("plugins.rose-pine"),
    require("plugins.autopairs"),
    require("plugins.lualine"),
    require("plugins.telescope"),
    require("plugins.neo-tree"),
    require("plugins.treesitter"),
    require("plugins.lsp-zero"),
    require("plugins.trouble"),
    require("plugins.rainbow"),
    require("plugins.smart-splits"),
    require("plugins.fugitive"),
    require("plugins.coqtail"),
    require("plugins.smoothie"),
    require("plugins.abolish"),
    require("plugins.vimtex"),
})
