return {
    "lervag/vimtex",
    lazy = false,
    init = function()
        vim.g.vimtex_view_method = "zathura"
        vim.g.vimtex_compiler_latexmk_engines = { ["_"] = '-lualatex'}
        vim.g.vimtex_compiler_latexmk = {
            options = {
                "-shell-escape",
                "-verbose",
                "-file-line-error",
                "-synctex=1",
                "-interaction=nonstopmode",
            },
        }

        vim.keymap.set("n", "<leader>vc", vim.cmd.VimtexCompile)
        vim.keymap.set("n", "<leader>vv", vim.cmd.VimtexView)
    end
}
