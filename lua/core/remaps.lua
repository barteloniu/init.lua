local keymap = vim.keymap.set

vim.g.mapleader = " "

-- terminal
keymap("n", "<leader>t", vim.cmd.term)
keymap("t", "<Esc>", "<C-\\><C-N>")
