-- colors
vim.opt.termguicolors = true
vim.opt.background = "light"

vim.opt.number = true

-- search
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.cursorline = true

-- indent with 4 spaces
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4  -- but also have reasonably wide tabs

-- disable mouse selection everywhere
vim.opt.mouse = ""

-- modes are already shown in status line
vim.opt.showmode = false

-- hide line numbers and gutter in terminal
vim.api.nvim_create_autocmd({ "TermOpen" }, {
    callback = function()
        vim.opt_local.number = false
        vim.opt_local.signcolumn = "no"
    end,
})
